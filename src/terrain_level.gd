extends Spatial


var dragging = false

export (String, FILE, "*.tscn") var sphere_node
export (String, FILE, "*.tscn") var sphere_line
export (String, FILE, "*.tscn") var sphere_arc
var DEBUG = true


var dist_btxt = 1.0

func _ready():
	pass # Replace with function body.

func draw_points(start_point: Vector3, end_point: Vector3):
	var vector = end_point-start_point
	
	var points = generate_points(start_point, dist_btxt, vector)
	for p in points:
		spawn_sphere(p, sphere_line)
	
func draw_spheres(points: Array, node):
	for p in points:
		spawn_sphere(p, node)
		
func draw_arc_spheres(points: Array):
	for p in points:
		spawn_sphere(p, sphere_arc)

func draw_terrain_points(start_point: Vector3, end_point: Vector3):
	var vector = end_point - start_point
	var points = generate_points(start_point, 1.0, vector)
	draw_spheres(points, sphere_line)
	var terrain_points = $TerrainPoints.points_along_terrain(points, 0.0, get_world())
	
	draw_spheres(terrain_points, sphere_node)

"""
Generates point a long a line from I to E
"""
func generate_points(start: Vector3, dist_btxt: float, vector: Vector3) -> Array:
	var point_size = 1.0
	var total_points = vector.length() / point_size
	var points = []
	
	for x in total_points:
		
		var distance_away = x * dist_btxt
		var point = start + (vector.normalized() * distance_away)
		points.append(point)
	return points

func spawn_sphere(point: Vector3, node):
	var sphere_scene = load(node)
	var sphere = sphere_scene.instance()
	sphere.translation = point
	add_child(sphere)

func rev(xs: Array):
	var rev_array = []
	for x in xs.size():
		var index = (xs.size() - 1) - x
		rev_array.append(xs[index])
	return rev_array
