extends Node

signal debug_line(i,e)

var DEBUG = true

var debug_overlay

export (float) var top_limit = 30
export (float) var bottom_limit = 30
export (float) var angle_change = 2

func _ready():
	debug_overlay = $"/root/DebugOverlay/DebugDraw3D"
	connect("debug_line",debug_overlay, "set_line")

func points_along_terrain(points: Array, angle: float, world: World) -> Array:
	var Ts = []
	var perpendicular = acquire_perpendicular(points.front(), points.back(),angle)

	Ts.append(points.front())
	
	for x in range(1,points.size()):
		var p0 = Ts[x-1]
		var p1 = points[x]
		
		var hover_distance = 0.2
		
		var p_level = level_point(p0, p1)
		var pt = Vector3(p_level.x,p_level.y+hover_distance, p_level.z)
		emit_signal("debug_line", pt,pt - (perpendicular*2))
		var result = world.direct_space_state.intersect_ray(pt, pt - (perpendicular*2))
		if result:
			emit_signal("debug_line",pt, result.position)
			var downward_vector = result.position - pt
			print_debug(downward_vector.length())
			if downward_vector.length() <= hover_distance+0.05:
				Ts.append(result.position)
			else:
				var arc_ps = arc_points(p0, p1, deg2rad(angle_change), deg2rad(top_limit), deg2rad(bottom_limit))
				if DEBUG:
					get_parent().draw_arc_spheres(arc_ps)
				var cp = collision_point(arc_ps, world)
				if cp != null:
					Ts.append(cp)
				else:
					printerr("Invalid points")
					return Ts
		else:
			pt = level_point(p0, p1)
			var arc_ps = arc_points(p0, pt, deg2rad(2), deg2rad(30), deg2rad(30))
			if DEBUG:
				get_parent().draw_arc_spheres(arc_ps)
			var cp = collision_point(arc_ps, world)
			if cp != null:
				Ts.append(cp)
			else:
				printerr("Invalid point")
				return Ts
	return Ts

func acquire_perpendicular(start, end, angle):
	var spherical_coord = to_spherical(start, end)
	var theta = spherical_coord[0]
	var phi = spherical_coord[1]
	var rho = spherical_coord[2]
	var p_perp = spherical_to_cartesian(theta - deg2rad(90), phi, rho)
	
	#var v_perp = p_perp - start
	
	return p_perp.normalized()
	
	
	
func level_point(p0, p1):
	var pl = Vector3(p1.x, p0.y, p1.z)
	return pl

func arc_points(p0: Vector3, p1: Vector3, change_per_step, top_limit, bottom_limit):
	var direction = p1 - p0
	var rho = direction.length()
	var theta = acos(direction.y/rho)
	var phi = atan2(direction.z,direction.x)
	
	var theta_top = theta - top_limit
	bottom_limit = abs(bottom_limit)
	var total_points = (top_limit+bottom_limit) / change_per_step
	var Ps = []
	for x in total_points:
		var angle_difference = x * change_per_step
		var sphere_vector = spherical_to_cartesian(theta_top+angle_difference, phi, rho)
		Ps.append(p0+sphere_vector)
	return Ps
	
func collision_point(ps: Array, world: World):
	var space_state = world.direct_space_state
	var p_prev = null
	for x in range(1,ps.size()):
		var p0 = ps[x-1]
		var p1 = ps[x]
		
		if DEBUG:
			debug_overlay.add_vector(p0,p1)
		
		var result = space_state.intersect_ray(p0,p1)
		
		if result.empty() == false:
			return result.position
			
		# Go up halg way
		if p_prev != null:
			var half_vector = (p0 - p_prev) / 2
			var half_point = p_prev + half_vector
			result = space_state.intersect_ray(half_point, p1)
			if result:
				return result.position
		#result = space_state.intersect_ray(top_point, p1)
		#if result:
		#	return result.position
		p_prev = p0
	return null

func to_spherical(p0, p1) -> Array:
	var direction = p1 - p0
	var rho = direction.length()
	var theta = acos(direction.y/rho)
	var phi = atan2(direction.z,direction.x)
	
	return [theta, phi, rho]

func spherical_to_cartesian(theta: float, phi: float, rho: float):
	var z = rho * sin(theta) * sin(phi)
	var y = rho * cos(theta)
	var x = rho * sin(theta) * cos(phi)
	
	var vector = Vector3(x, y, z)
	
	return vector
